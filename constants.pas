unit constants;

{$mode objfpc}{$H+}

interface

const
  { Constants }
  EMPTY = -1047;
  CONT_W = 40;
  CONT_H = 30;

  { Constraints }
  MAX_ITEM = 999;
  MAX_SIZE = 32;
  MAX_FUN = 2;

  { Exceptions }
  ALREADY_INSERTED = '> Cannot insert the same number twice.';
  NO_SUCH_FILE = '> File does not exist.';
  NO_SUCH_NUMBER = '> Cannot delete - no such number in hash map.';
  // NO_SUCH_FUNCTION = '> ';
  WRONG_INPUT = '> Please enter a non-negative integer smaller than 1000.';
  WRONG_FILE_FORMAT = '> Aborting - invalid file format.';
  INVALID_INTERVAL = '> Please enter a valid interval from 0 to 999.';
  NO_SPACE_LEFT = '> Space limit reached - cannot insert new items unless some of the old ones are deleted.';
  NO_ITEM_LEFT = '> Every single number from 0 to 999 inserted, cannot add any more numbers.';

  { Information }
  MAP_CREATED = '> Hash map created.';
  ITEMS_INSERTED = '> New items have been inserted.';
  ITEM_INSERTED = '> Item inserted.';
  ITEM_DELETED = '> Item deleted.';
  ITEM_FOUND = '> Hash map contains that number.';
  ITEM_NOT_FOUND = '> There is no such number in the hash map.';

  { Stats }
  MAX_LIST_SIZE = '> Maximum size: ';
  MIN_LIST_SIZE = '> Minimum size: ';
  AVG_LIST_SIZE = '> Average size: ';
  ITEMS = '> Items: ';
  COLLISIONS = '> Collisions: ';
  LAST_OPERATION_TIME = '> Last operation: ';

  HX = 'h(x) = ';
  OPEN = 'open';
  CLOSED = 'closed';
  NA = 'N/A';

implementation

end.

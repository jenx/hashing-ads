unit hash;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, constants;

type
  TFun = function(const Item: integer): integer of object;

  THash = class abstract
    protected
      PFun: TFun;
      PFunTab: array [0..MAX_FUN - 1] of TFun;
      PSize: integer;
      PImg: TImage;
      PCollisions: integer;
      PActive: boolean;

      { Hash Functions }
      function Hash0(const Item: integer): integer;
      function Hash1(const Item: integer): integer;
      function FunToInt(Fun: TFun): integer;
      function IntToFun(const Num: integer): TFun;

      { Property Getters }
      function GetCollisions: integer;
      function GetSize: integer;
      function GetFun: integer;
      function GetItems: integer; virtual; abstract;

    public
      constructor Create; virtual;
      constructor Create(const Fun, Amount: integer; const Where: TImage); virtual; abstract;
      constructor Create(const Path: string; const Where: TImage); virtual; abstract;
      destructor Destroy; virtual; abstract;

      procedure Draw; virtual; abstract;
      procedure Save(const Path: string); virtual; abstract;
      procedure Insert(const Item: integer); virtual; abstract;
      procedure Delete(const Item: integer); virtual; abstract;
      function Member(const Item: integer): boolean; virtual; abstract;
      function InsertCheck(const Amount: integer): integer; virtual; abstract;

      property Size: integer read GetSize;
      property Items: integer read GetItems;
      property Collisions: integer read GetCollisions;
      property Fun: integer read GetFun;
  end;

implementation

function THash.GetFun: integer;
begin
  Result := FunToInt(PFun);
end;

function THash.Hash0(const Item: integer): integer;
begin
  Result := Item mod PSize;
end;

function THash.Hash1(const Item: integer): integer;
begin
  Result := (Item + 1) mod PSize;
end;

function THash.FunToInt(Fun: TFun): integer;
begin
  if Fun = @Hash0 then
    Result := 0;
  if Fun = @Hash1 then
    Result := 1;
end;

function THash.IntToFun(const Num: integer): TFun;
begin
  case Num of
    0:
      Result := @Hash0;
    1:
      Result := @Hash1;
  end;
end;

function THash.GetCollisions: integer;
begin
  Result := PCollisions;
end;

function THash.GetSize: integer;
begin
  Result := PSize;
end;

constructor THash.Create;
begin
  PFunTab[0] := @Hash0;
  PFunTab[1] := @Hash1;
end;

end.

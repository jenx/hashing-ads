unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls, Menus, Buttons, constants, hash, openhash,
  closedhash, newmap;

type

  { TFMain }

  TFMain = class(TForm)
      BInsert: TBitBtn;
      BDelete: TBitBtn;
      BInsertRandom: TBitBtn;
      BMember: TBitBtn;
      BQuit: TBitBtn;
      BCreate: TButton;
      BRandomize: TButton;
      CActive1: TCheckBox;
      CActive2: TCheckBox;
      G2: TGroupBox;
      GeneralProps2: TGroupBox;
      GEvents2: TGroupBox;
      GOperation: TGroupBox;
      GInput: TGroupBox;
      G1: TGroupBox;
      GHash1: TGroupBox;
      GHash2: TGroupBox;
      GWelcome: TGroupBox;
      GStats1: TGroupBox;
      GStats2: TGroupBox;
      IMain2: TImage;
      LWelcome1: TLabel;
      LAvgListSize2: TLabel;
      LCollisions2: TLabel;
      LFunction2: TLabel;
      LFunctionCap2: TLabel;
      ListProps2: TGroupBox;
      LItems1: TLabel;
      LCollisions1: TLabel;
      LItems2: TLabel;
      LMaxListSize2: TLabel;
      LMinListSize2: TLabel;
      LRehash2: TLabel;
      LRehashCap2: TLabel;
      LSize2: TLabel;
      LSizeCap2: TLabel;
      LType2: TLabel;
      LSize1: TLabel;
      LTypeCap2: TLabel;
      MainMenu: TMainMenu;
      MFile: TMenuItem;
      MQuit: TMenuItem;
      MNew: TMenuItem;
      Messages2: TMemo;
      Props1: TGroupBox;
      LAvgListSize1: TLabel;
      ListProps1: TGroupBox;
      LMaxListSize1: TLabel;
      LMinListSize1: TLabel;
      LTypeCap1: TLabel;
      LFunctionCap1: TLabel;
      LRehashCap1: TLabel;
      LType1: TLabel;
      LFunction1: TLabel;
      LRehash1: TLabel;
      LSizeCap1: TLabel;
      LNumber: TLabel;
      Props2: TGroupBox;
      GEvents1: TGroupBox;
      IMain1: TImage;
      LAmount: TLabel;
      Messages1: TMemo;
      ScrollBox1: TScrollBox;
      ScrollBox2: TScrollBox;
      TBAmount: TTrackBar;
      TBInput: TTrackBar;
      GeneralProps1: TGroupBox;

      procedure FormCreate(Sender: TObject);
      procedure BCreateClick(Sender: TObject);
      procedure BInsertClick(Sender: TObject);
      procedure BInsertRandomClick(Sender: TObject);
      procedure BDeleteClick(Sender: TObject);
      procedure BMemberClick(Sender: TObject);
      procedure MNewClick(Sender: TObject);

      procedure TBInputChange(Sender: TObject);
      procedure TBAmountChange(Sender: TObject);
      procedure GHash1Click(Sender: TObject);
      procedure GHash2Click(Sender: TObject);
      procedure BRandomizeClick(Sender: TObject);
      procedure CActive1Change(Sender: TObject);
      procedure CActive2Change(Sender: TObject);
      procedure MQuitClick(Sender: TObject);
      procedure BQuitClick(Sender: TObject);

    private
      procedure StatsUpdate;
      procedure BoxUpdate;
      procedure Quit;
  end;

var
  FMain: TFMain;
  Hash1: THash;
  Hash2: THash;

implementation

{$R *.lfm}

{ Main Functionality }

procedure TFMain.FormCreate(Sender: TObject);
begin
  LAmount.Caption := IntToStr(TBAmount.Position);
  LNumber.Caption := IntToStr(TBInput.Position);
  Messages1.Lines.Clear;
  Messages2.Lines.Clear;
end;

procedure TFMain.BCreateClick(Sender: TObject);
begin
  if FNewMap.ShowModal = mrOk then
  begin
    GWelcome.Visible := False;
    GInput.Enabled := True;
    GOperation.Enabled := True;
    GHash1.Visible := True;
    CActive1.Checked := True;

    { Displaying a single map }
    if not FNewMap.Compare then
    begin
      CActive2.Checked := False;
      GHash2.Visible := False;
      GHash1.Height := 750;
      BInsertRandom.Visible := True;
      TBAmount.Visible := True;
      LAmount.Visible := True;
    end
    { Displaying two maps }
    else
    begin
      CActive2.Checked := True;
      GHash2.Visible := True;
      GHash1.Height := 360;
      BInsertRandom.Visible := False;
      TBAmount.Visible := False;
      LAmount.Visible := False;
    end;

    { Create Hash1 }
    if FNewMap.Open1 then
    begin
      Hash1 := TOpenHash.Create(FNewMap.Function1, FNewMap.Size1, IMain1);
      LSize1.Caption := IntToStr(Hash1.Size);
      LType1.Caption := OPEN;
      LFunction1.Caption := HX + 'x mod ' + IntToStr(Hash1.Size);
      LRehashCap1.Visible := False;
    end
    else
    begin
      Hash1 := TClosedHash.Create(FNewMap.Function1, FNewMap.Size1, IMain1,
                                  FNewMap.Linear1, FNewMap.CL1, FNewMap.CQ1);
      LSize1.Caption := IntToStr(Hash1.Size);
      LType1.Caption := CLOSED;
      LFunction1.Caption := HX + 'x mod ' + IntToStr(Hash1.Size);
      if FNewMap.Linear1 then
        LRehash1.Caption := HX + 'h(x) + 1'
      else
        LRehash1.Caption := HX + 'h(x) ' + IntToStr((Hash1 as TClosedHash).CQ) +
                            'I^2 + ' + IntToStr((Hash1 as TClosedHash).CL) +
                            'I';
    end;

    { Create Hash2 }
    if FNewMap.Compare then
      if FNewMap.Open2 then
      begin
        Hash2 := TOpenHash.Create(FNewMap.Function2, FNewMap.Size2, IMain2);
        LSize2.Caption := IntToStr(Hash2.Size);
        LType2.Caption := OPEN;
        LFunction2.Caption := HX + 'x mod ' + IntToStr(Hash2.Size);
        LRehashCap2.Visible := False;
      end
      else
      begin
        Hash2 := TClosedHash.Create(FNewMap.Function2, FNewMap.Size2,
          IMain2, FNewMap.Linear2, FNewMap.CL2, FNewMap.CQ2);
        LSize2.Caption := IntToStr(Hash2.Size);
        LType2.Caption := 'closed';
        LFunction2.Caption := HX + 'x mod ' + IntToStr(Hash2.Size);
        if FNewMap.Linear2 then
          LRehash2.Caption := HX + 'h(x) + 1'
        else
          LRehash2.Caption := HX + 'h(x) ' +
                              IntToStr((Hash2 as TClosedHash).CQ) +  'I^2 + ' +
                              IntToStr((Hash2 as TClosedHash).CL) + 'I';
      end;
    StatsUpdate;
  end;
end;

procedure TFMain.BInsertClick(Sender: TObject);
begin
  if CActive1.Checked then
    try
      Hash1.Insert(TBInput.Position);
      Messages1.Lines.Append(ITEM_INSERTED);
    except on E: Exception do
      Messages1.Lines.Append(E.Message);
    end;
  if CActive2.Checked then
    try
      Hash2.Insert(TBInput.Position);
      Messages2.Lines.Append(ITEM_INSERTED);
    except on E: Exception do begin
      Messages2.Lines.Append(E.Message);
      Exit;
      end
    end;
  TBInput.SetFocus;
  BoxUpdate;
  StatsUpdate;
end;

procedure TFMain.BInsertRandomClick(Sender: TObject);
var
  Hash: THash;
  Amount, I, R: integer;
begin
  if CActive1.Checked then
    Hash := Hash1;
  if CActive2.Checked then
    Hash := Hash2;
  if Hash = nil then
    Exit;
  Amount := Hash.InsertCheck(TBAmount.Position);
  { Check whether we're trying to add more than possible }
  if Amount <= 0 then
    if Hash is TOpenHash then
      Messages1.Lines.Append(NO_ITEM_LEFT)
    else
      Messages1.Lines.Append(NO_SPACE_LEFT)
  else
  begin
    Randomize;
    { If OpenHash, add and track random items using PUnused (TSet) }
    if Hash is TOpenHash then
      for I := 1 to Amount do
        (Hash as TOpenHash).Insert
    { If ClosedHash, then generate random items in place }
    else
      for I := 1 to Amount do
      begin
        R := Random(MAX_ITEM) + 1;
        while Hash.Member(R) do
          R := Random(MAX_ITEM) + 1;
        Hash.Insert(R);
      end;

    if Amount = 1 then
      Messages1.Append(ITEM_INSERTED)
    else
      Messages1.Append(ITEMS_INSERTED);
  end;
  TBInput.SetFocus;
  BoxUpdate;
  StatsUpdate;
end;

procedure TFMain.BDeleteClick(Sender: TObject);
begin
  if CActive1.Checked then
    try
      Hash1.Delete(TBInput.Position);
      Messages1.Lines.Append(ITEM_DELETED);
    except on E: Exception do
      Messages1.Lines.Append(E.Message);
    end;
  if CActive2.Checked then
    try
      Hash2.Delete(TBInput.Position);
      Messages2.Lines.Append(ITEM_DELETED);
    except on E: Exception do
      Messages2.Lines.Append(E.Message);
    end;
  TBInput.SetFocus;
  BoxUpdate;
  StatsUpdate;
end;

procedure TFMain.BMemberClick(Sender: TObject);
begin
  if CActive1.Checked then
    if Hash1.Member(TBInput.Position) then
      Messages1.Lines.Append(ITEM_FOUND)
    else
      Messages1.Lines.Append(ITEM_NOT_FOUND);
  if CActive2.Checked then
    if Hash2.Member(TBInput.Position) then
      Messages2.Lines.Append(ITEM_FOUND)
    else
      Messages2.Lines.Append(ITEM_NOT_FOUND);
  TBInput.SetFocus;
end;

procedure TFMain.MNewClick(Sender: TObject);
begin
  BCreateClick(Self);
end;

procedure TFMain.StatsUpdate;
begin
  if CActive1.Checked then
  begin
    if Hash1 is TClosedHash then
    begin
      LMinListSize1.Caption := MIN_LIST_SIZE + NA;
      LMaxListSize1.Caption := MAX_LIST_SIZE + NA;
      LAvgListSize1.Caption := AVG_LIST_SIZE + NA;
    end
    else
    begin
      LMinListSize1.Caption := MIN_LIST_SIZE +
                               IntToStr((Hash1 as TOpenHash).MinListSize);
      LMaxListSize1.Caption := MAX_LIST_SIZE +
                               IntToStr((Hash1 as TOpenHash).MaxListSize);
      LAvgListSize1.Caption := AVG_LIST_SIZE +
                               FloatToStr((Hash1 as TOpenHash).AvgListSize);
    end;
    LItems1.Caption := ITEMS + IntToStr(Hash1.Items);
    LCollisions1.Caption := COLLISIONS + IntToStr(Hash1.Collisions);
  end;

  if CActive2.Checked then
  begin
    if Hash2 is TClosedHash then
    begin
      LMinListSize2.Caption := MIN_LIST_SIZE + NA;
      LMaxListSize2.Caption := MAX_LIST_SIZE + NA;
      LAvgListSize2.Caption := AVG_LIST_SIZE + NA;
    end
    else
    begin
      LMinListSize2.Caption := MIN_LIST_SIZE +
                               IntToStr((Hash2 as TOpenHash).MinListSize);
      LMaxListSize2.Caption := MAX_LIST_SIZE +
                               IntToStr((Hash2 as TOpenHash).MaxListSize);
      LAvgListSize2.Caption := AVG_LIST_SIZE +
                               FloatToStr((Hash2 as TOpenHash).AvgListSize);
    end;
    LItems2.Caption := ITEMS + IntToStr(Hash2.Items);
    LCollisions2.Caption := COLLISIONS + IntToStr(Hash2.Collisions);
  end;
end;

procedure TFMain.BoxUpdate;
var
  Max: integer;
begin
  if Hash1 is TOpenHash then
  begin
    Max := CONT_H * (Hash1 as TOpenHash).MaxListSize;
    IMain1.Height := Max + 147;
    IMain1.Picture.Bitmap.Height := IMain1.Height;
    IMain1.Canvas.Pen.Color := clWhite;
    IMain1.Canvas.Rectangle(0, Max + 47, IMain1.Width, IMain1.Height);
  end;
  if Hash2 is TOpenHash then
  begin
    Max := CONT_H * (Hash2 as TOpenHash).MaxListSize;
    IMain2.Height := Max + 147;
    IMain2.Picture.Bitmap.Height := IMain2.Height;
    IMain2.Canvas.Pen.Color := clWhite;
    IMain2.Canvas.Rectangle(0, Max + 47, IMain2.Width, IMain2.Height);
  end;
end;

{ Form Behavior }

procedure TFMain.TBInputChange(Sender: TObject);
begin
  LNumber.Caption := IntToStr(TBInput.Position);
end;

procedure TFMain.TBAmountChange(Sender: TObject);
begin
  LAmount.Caption := IntToStr(TBAmount.Position);
end;

procedure TFMain.GHash1Click(Sender: TObject);
begin
  CActive1.Checked := not CActive1.Checked;
end;

procedure TFMain.GHash2Click(Sender: TObject);
begin
  CActive2.Checked := not CActive2.Checked;
end;

procedure TFMain.MQuitClick(Sender: TObject);
begin
  Quit;
end;

procedure TFMain.BQuitClick(Sender: TObject);
begin
  Quit;
end;

procedure TFMain.BRandomizeClick(Sender: TObject);
begin
  Randomize;
  TBInput.Position := Random(999) + 1;
end;

procedure TFMain.CActive1Change(Sender: TObject);
begin
  if CActive1.Checked then
    G1.Color := clTeal
  else
    G1.Color := clDefault;
end;

procedure TFMain.CActive2Change(Sender: TObject);
begin
  if CActive2.Checked then
    G2.Color := clTeal
  else
    G2.Color := clDefault;
end;

procedure TFMain.Quit;
begin
  if Hash1 <> nil then
  begin
    Hash1.Free;
    Hash1 := nil;
  end;
  if Hash2 <> nil then
  begin
    Hash2.Free;
    Hash2 := nil;
  end;
  Self.Destroy;
end;

end.

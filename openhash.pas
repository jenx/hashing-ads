unit openhash;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, Graphics, constants, hash, linkedlist;

type
  TSet = class
    Numbers: array [0..MAX_ITEM] of integer;
    Size: integer;

    constructor Create;
    function Add(const What: integer): boolean;
    function Find(const What: integer): integer;
    function Pop: integer;
    function RemoveValue(const What: integer): boolean;
    procedure RemoveIndex(const Index: integer);
  end;

  TOpenHash = class(THash)
    private
      PTab: array of TLinkedList;
      PListSizes: array [0..MAX_SIZE - 1] of integer;
      PUnused: TSet;
      PAvailable: integer;

      function GetItems: integer; override;
      function GetAvgListSize: integer;
      function GetMaxListSize: integer;
      function GetMinListSize: integer;

    public
      constructor Create(const Path: string; const Where: TImage); override;
      constructor Create(const Func, Amount: integer; const Where: TImage); override;
      destructor Destroy; override;

      procedure Draw; override;
      procedure Save(const Path: string); override;
      procedure Insert(const Item: integer); override;
      procedure Insert;
      procedure Delete(const Item: integer); override;
      function Member(const Item: integer): boolean; override;
      function InsertCheck(const Amount: integer): integer; override;

      property Items: integer read GetItems;
      property MinListSize: integer read GetMinListSize;
      property MaxListSize: integer read GetMaxListSize;
      property AvgListSize: integer read GetAvgListSize;
  end;

implementation

{ TSet }

constructor TSet.Create;
var
  I: integer;
begin
  Self.Size := MAX_ITEM + 1;
  for I := 0 to High(Numbers) do
    Numbers[I] := I;
end;

function TSet.Find(const What: integer): integer;
var
  I: integer;
  Found: boolean = False;
begin
  for I := 0 to Self.Size - 1 do
    if Numbers[I] = What then
    begin
      Found := True;
      Break;
    end;
  if Found then
    Result := I
  else
    Result := -1;
end;

function TSet.Pop: integer;
var
  R: integer;
begin
  Randomize;
  R := Random(Self.Size);
  Result := Numbers[R];
  RemoveIndex(R);
end;

function TSet.RemoveValue(const What: integer): boolean;
var
  Index: integer;
begin
  Index := Find(What);
  if Index <> -1 then
  begin
    RemoveIndex(Index);
    Result := True;
  end
  else
    Result := False;
end;

procedure TSet.RemoveIndex(const Index: integer);
begin
  Numbers[Index] := Numbers[Self.Size - 1];
  Numbers[Self.Size - 1] := -1;
  Dec(Self.Size);
end;

function TSet.Add(const What: integer): boolean;
var
  Index: integer;
begin
  Index := Find(What);
  if Index = -1 then
  begin
    Numbers[Self.Size] := What;
    Inc(Self.Size);
    Result := True;
  end
  else
    Result := False;
end;

{ TOpenHash }

constructor TOpenHash.Create(const Func, Amount: integer; const Where: TImage);
var
  I: integer;
begin
  inherited Create;
  SetLength(PTab, Amount);
  for I := 0 to High(PTab) do
    PTab[I] := TLinkedList.Create;
  for I := 0 to High(PListSizes) do
    PListSizes[I] := 0;
  PUnused := TSet.Create;
  PFun := IntToFun(Func);
  PSize := Amount;
  PImg := Where;
  PAvailable := MAX_ITEM;
  PCollisions := 0;
  PActive := True;
  Draw;
end;

destructor TOpenHash.Destroy;
var
  I: integer;
begin
  for I := 0 to High(PTab) do
    PTab[I].Free;
  SetLength(PTab, 0);
end;

constructor TOpenHash.Create(const Path: string; const Where: TImage);
var
  Source: TStream;
begin
  inherited Create;
  PImg := Where;
  try
    Source := TFileStream.Create(Path, fmOpenRead);
  finally
    Source.Free;
  end;
end;

procedure TOpenHash.Save(const Path: string);
var
  Dest: TStream;
begin
  try
    Dest := TFileStream.Create(Path, fmCreate);
  finally
    Dest.Free;
  end;
end;

procedure TOpenHash.Insert(const Item: integer);
var
  Index: integer;
begin
  if not PUnused.RemoveValue(Item) then
    raise Exception.Create(ALREADY_INSERTED);
  Index := PFun(Item);
  if PTab[Index].Append(Item) then
    Inc(PCollisions);
  Inc(PListSizes[Index]);
  Dec(PAvailable);
  Draw;
end;

procedure TOpenHash.Insert;
var
  Item, Index: integer;
begin
  Item := PUnused.Pop;
  Index := PFun(Item);
  if PTab[Index].Append(Item) then
    Inc(PCollisions);
  Inc(PListSizes[Index]);
  Dec(PAvailable);
  Draw;
end;

procedure TOpenHash.Delete(const Item: integer);
var
  Index: integer;
begin
  if not PUnused.Add(Item) then
    raise Exception.Create(NO_SUCH_NUMBER);
  Index := PFun(Item);
  PTab[Index].Delete(Item);
  Dec(PListSizes[Index]);
  Inc(PAvailable);
  Draw;
end;

procedure TOpenHash.Draw;
var
  C, R: integer;
begin
  PImg.Canvas.FillRect(PImg.ClientRect);
  for C := 0 to High(PTab) do
  begin
    PImg.Canvas.Font.Bold := True;
    PImg.Canvas.TextOut(C * CONT_W + (PImg.Width - PSize * CONT_W) div 2 - 5,
                        10, IntToStr(C));
    for R := 1 to PListSizes[C] do
      with PImg.Canvas do
      begin
        Font.Bold := False;
        Pen.Color := clLtGray;
        Rectangle(C * CONT_W + (PImg.Width - PSize * CONT_W) div 2 - 20,
                  R * CONT_H + 20 - 10,
                  C * CONT_W + (PImg.Width - PSize * CONT_W) div 2 + 21,
                  R * CONT_H + 20 + 21);
        TextOut(C * CONT_W + (PImg.Width - PSize * CONT_W) div 2 - 11,
                R * CONT_H + 19, PTab[C].Text[R - 1]);
      end;
  end;
end;

function TOpenHash.GetItems: integer;
begin
  Result := MAX_ITEM - PAvailable;
end;

function TOpenHash.GetAvgListSize: integer;
var
  I: integer;
begin
  Result := 0;
  for I := 0 to PSize - 1 do
    Result := Result + PListSizes[I];
  Result := Result div PSize;
end;

function TOpenHash.GetMaxListSize: integer;
var
  I: integer;
begin
  Result := -1;
  for I := 0 to PSize - 1 do
    if PListSizes[I] > Result then
      Result := PListSizes[I];
end;

function TOpenHash.GetMinListSize: integer;
var
  I: integer;
begin
  Result := MAX_ITEM + 1;
  for I := 0 to PSize - 1 do
    if PListSizes[I] < Result then
      Result := PListSizes[I];
end;

function TOpenHash.Member(const Item: integer): boolean;
var
  Index: integer;
begin
  Result := False;
  Index := PFun(Item);
  Result := PTab[Index].Member(Item);
end;

function TOpenHash.InsertCheck(const Amount: integer): integer;
begin
  Result := Amount;
  if PAvailable < Amount then
    Result := PAvailable;
end;

end.

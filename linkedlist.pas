unit linkedlist;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, constants;

type

  TValues = array [0..MAX_ITEM - 1] of string [3];

  TNode = class
    Value: integer;
    Next: TNode;

    constructor Create(Val: integer);
    destructor Destroy; override;
    function Text: string;
  end;

  TLinkedList = class
    private
      First: TNode;
      Last: TNode;

    public
      constructor Create;
      destructor Destroy; override;
      procedure Delete(Item: integer);
      function Append(Item: integer): boolean;
      function Member(Item: integer): boolean;
      function Text: TValues;
  end;

implementation

{ TNode }

constructor TNode.Create(Val: integer);
begin
  Value := Val;
  Next := nil;
end;

destructor TNode.Destroy;
begin
  Next := nil;
end;

function TNode.Text: string;
begin
  Result := IntToStr(Value);
end;

{ TLinkedList }

constructor TLinkedList.Create;
begin
  First := nil;
  Last := nil;
end;

destructor TLinkedList.Destroy;
begin
  inherited Destroy;
end;

function TLinkedList.Append(Item: integer): boolean;
{ Returns True if there was a collision; False otherwise. }
begin
  Result := Assigned(First);
  if not Assigned(First) then
  begin
    First := TNode.Create(Item);
    Last := First;
  end
  else
  begin
    Last.Next := TNode.Create(Item);
    Last := Last.Next;
  end;
end;

procedure TLinkedList.Delete(Item: integer);
var
  Node: TNode;
  OldNode: TNode;
begin
  Node := First;
  if not Assigned(Node) then
    Exit;
  if Node.Value = Item then
  begin
    First := Node.Next;
    Node.Free;
  end
  else
    while Node.Next <> nil do
    begin
      if Node.Next.Value = Item then
      begin
        OldNode := Node.Next;
        Node.Next := Node.Next.Next;
        if Node.Next = nil then
          Last := Node;
        OldNode.Free;
        Break;
      end;
      Node := Node.Next;
    end;
end;

function TLinkedList.Member(Item: integer): boolean;
var
  Node: TNode;
begin
  Result := False;
  Node := First;
  while Node <> nil do
  begin
    if Node.Value = Item then
    begin
      Result := True;
      Break;
    end;
    Node := Node.Next;
  end;
end;

function TLinkedList.Text: TValues;
var
  Node: TNode;
  I: integer = 0;
begin
  Node := First;
  while Node <> nil do
  begin
    Result[I] := Node.Text;
    Node := Node.Next;
    Inc(I);
  end;
end;

end.

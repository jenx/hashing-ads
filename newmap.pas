unit newmap;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, ExtCtrls, Buttons;

type

  { TFNewMap }

  TFNewMap = class(TForm)
      BOK: TButton;
      TCL1: TTrackBar;
      TCQ1: TTrackBar;
      TCQ2: TTrackBar;
      TCL2: TTrackBar;
      GConstants1: TGroupBox;
      GConstants2: TGroupBox;
      G1: TGroupBox;
      G2: TGroupBox;
      GSize1: TGroupBox;
      GSize2: TGroupBox;
      Label1: TLabel;
      Label2: TLabel;
      LC1: TLabel;
      LC2: TLabel;
      LC3: TLabel;
      LC4: TLabel;
      LTrackbarPos1: TLabel;
      LTrackbarPos2: TLabel;
      RBFun11: TRadioButton;
      RBFun12: TRadioButton;
      RBClosed2: TRadioButton;
      RBLinear2: TRadioButton;
      RBOpen2: TRadioButton;
      RBQuadratic2: TRadioButton;
      RBCompare: TRadioButton;
      RFunction2: TRadioGroup;
      RRehash2: TRadioGroup;
      RBSingle: TRadioButton;
      RMode: TRadioGroup;
      RBClosed1: TRadioButton;
      RBLinear1: TRadioButton;
      RBOpen1: TRadioButton;
      RBQuadratic1: TRadioButton;
      RFunction1: TRadioGroup;
      RRehash1: TRadioGroup;
      RType1: TRadioGroup;
      RType2: TRadioGroup;
      TBSize1: TTrackBar;
      TBSize2: TTrackBar;
      procedure FormCreate(Sender: TObject);
      procedure BOKClick(Sender: TObject);
      procedure RBSingleChange(Sender: TObject);
      procedure TBSize1Change(Sender: TObject);
      procedure TBSize2Change(Sender: TObject);
      procedure RBClosed1Change(Sender: TObject);
      procedure RBClosed2Change(Sender: TObject);
      procedure RBQuadratic1Change(Sender: TObject);
      procedure RBQuadratic2Change(Sender: TObject);

    private
      function GetCompare: boolean;
      function GetSize1: integer;
      function GetSize2: integer;
      function GetOpen1: boolean;
      function GetOpen2: boolean;
      function GetFunction1: integer;
      function GetFunction2: integer;
      function GetLinear1: boolean;
      function GetLinear2: boolean;
      function GetCL1: integer;
      function GetCQ1: integer;
      function GetCL2: integer;
      function GetCQ2: integer;

    public
      property Compare: boolean read GetCompare;
      property Size1: integer read GetSize1;
      property Size2: integer read GetSize2;
      property Open1: boolean read GetOpen1;
      property Open2: boolean read GetOpen2;
      property Function1: integer read GetFunction1;
      property Function2: integer read GetFunction2;
      property Linear1: boolean read GetLinear1;
      property Linear2: boolean read GetLinear2;
      property CL1: integer read GetCL1;
      property CQ1: integer read GetCQ1;
      property CL2: integer read GetCL2;
      property CQ2: integer read GetCQ2;
  end;

var
  FNewMap: TFNewMap;

implementation

{$R *.lfm}

{ Form Behavior }

procedure TFNewMap.FormCreate(Sender: TObject);
begin
  LTrackbarPos1.Caption := IntToStr(TBSize1.Position);
  LTrackbarPos2.Caption := IntToStr(TBSize2.Position);
end;

procedure TFNewMap.BOKClick(Sender: TObject);
begin
  Self.Hide;
end;

procedure TFNewMap.RBSingleChange(Sender: TObject);
begin
  G2.Enabled := not RBSingle.Checked;
end;

procedure TFNewMap.TBSize1Change(Sender: TObject);
begin
  LTrackbarPos1.Caption := IntToStr(TBSize1.Position);
end;

procedure TFNewMap.TBSize2Change(Sender: TObject);
begin
  LTrackbarPos2.Caption := IntToStr(TBSize2.Position);
end;

procedure TFNewMap.RBClosed1Change(Sender: TObject);
begin
  RRehash1.Visible := RBClosed1.Checked;
end;

procedure TFNewMap.RBClosed2Change(Sender: TObject);
begin
  RRehash2.Visible := RBClosed2.Checked;
end;

procedure TFNewMap.RBQuadratic1Change(Sender: TObject);
begin
  GConstants1.Visible := RBQuadratic1.Checked;
end;

procedure TFNewMap.RBQuadratic2Change(Sender: TObject);
begin
  GConstants2.Visible := RBQuadratic2.Checked;
end;

{ Property Getters }

function TFNewMap.GetCompare: boolean;
begin
  Result := RBCompare.Checked;
end;

function TFNewMap.GetSize1: integer;
begin
  Result := TBSize1.Position;
end;

function TFNewMap.GetSize2: integer;
begin
  Result := TBSize2.Position;
end;

function TFNewMap.GetOpen1: boolean;
begin
  Result := RBOpen1.Checked;
end;

function TFNewMap.GetOpen2: boolean;
begin
  Result := RBOpen2.Checked;
end;

function TFNewMap.GetFunction1: integer;
begin
  { Can't get ItemIndex to work properly, using a workaround... }
  if RBFun11.Checked then
    Result := 0;
  { If another hash function is added in the future...
  if RBFun21.Checked then
    Result := 1;
  }
end;

function TFNewMap.GetFunction2: integer;
begin
  { Can't get ItemIndex to work properly, using a workaround... }
  if RBFun12.Checked then
    Result := 0;
  { If another hash function is added in the future...
  if RBFun22.Checked then
    Result := 1;
  }
end;

function TFNewMap.GetLinear1: boolean;
begin
  Result := RBLinear1.Checked;
end;

function TFNewMap.GetLinear2: boolean;
begin
  Result := RBLinear2.Checked;
end;

function TFNewMap.GetCL1: integer;
begin
  Result := TCL1.Position;
end;

function TFNewMap.GetCL2: integer;
begin
  Result := TCL2.Position;
end;

function TFNewMap.GetCQ2: integer;
begin
  Result := TCQ2.Position;
end;

function TFNewMap.GetCQ1: integer;
begin
  Result := TCQ1.Position;
end;

end.

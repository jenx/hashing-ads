unit closedhash;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, Graphics, hash, constants;

type
  TClosedHash = class(THash)
    private
      PTab: array of integer;
      PUsed: integer;
      PLinear: boolean;
      PCL: integer;
      PCQ: integer;

      function GetCL: integer;
      function GetCQ: integer;
      function GetItems: integer; override;

    public
      constructor Create(const Func, Amount: integer; const Where: TImage; Linear: boolean; CL, CQ: integer);
      constructor Create(const Func, Amount: integer; const Where: TImage); override;
      constructor Create(const Path: string; const Where: TImage); override;
      destructor Destroy; override;

      procedure Draw; override;
      procedure Save(const Path: string); override;
      procedure Insert(const Item: integer); override;
      procedure Delete(const Item: integer); override;
      function Member(const Item: integer): boolean; override;
      function InsertCheck(const Amount: integer): integer; override;

      property CL: integer read GetCL;
      property CQ: integer read GetCQ;
      property Size: integer read GetSize;
      property Items: integer read GetItems;
      property Collisions: integer read GetCollisions;
  end;

implementation

function TClosedHash.GetItems: integer;
begin
  Result := PUsed;
end;

function TClosedHash.GetCQ: integer;
begin
  Result := PCQ;
end;

function TClosedHash.GetCL: integer;
begin
  Result := PCL;
end;

constructor TClosedHash.Create(const Func, Amount: integer; const Where: TImage;
  Linear: boolean; CL, CQ: integer);
begin
  PLinear := Linear;
  PCL := CL;
  PCQ := CQ;
  Create(Func, Amount, Where);
end;

constructor TClosedHash.Create(const Func, Amount: integer; const Where: TImage);
var
  I: integer;
begin
  inherited Create;
  PSize := Amount;
  SetLength(PTab, PSize);
  for I := 0 to High(PTab) do
    PTab[I] := EMPTY;
  PFun := IntToFun(Func);
  PImg := Where;
  PUsed := 0;
  PCollisions := 0;
  PActive := True;
  Draw;
end;

constructor TClosedHash.Create(const Path: string; const Where: TImage);
var
  Source: TStream;
begin
  inherited Create;
  PImg := Where;
  try
    Source := TFileStream.Create(Path, fmOpenRead);
  finally
    Source.Free;
  end;
end;

destructor TClosedHash.Destroy;
begin
  SetLength(PTab, 0);
end;

procedure TClosedHash.Draw;
var
  I: integer;
begin
  PImg.Canvas.FillRect(PImg.ClientRect);
  for I := 0 to High(PTab) do
  begin
    PImg.Canvas.Font.Bold := True;
    PImg.Canvas.TextOut(I * CONT_W + (PImg.Width - PSize * CONT_W) div 2 - 5,
                        10, IntToStr(I));
    if PTab[I] <> EMPTY then
      with PImg.Canvas do
      begin
        if PTab[I] < 0 then
          Font.Color := clRed;
        Font.Bold := False;
        Pen.Color := clLtGray;
        Rectangle(I * CONT_W + (PImg.Width - PSize * CONT_W) div 2 - 20,
                  40,
                  I * CONT_W + (PImg.Width - PSize * CONT_W) div 2 + 21,
                  CONT_H + 40);
        TextOut(I * CONT_W + (PImg.Width - PSize * CONT_W) div 2 - 11,
                CONT_H + 18,
                IntToStr(PTab[I]));
        Font.Color := clBlack;
      end;
  end;
end;

procedure TClosedHash.Save(const Path: string);
var
  Dest: TStream;
begin
  Dest := TFileStream.Create(Path, fmCreate);
  Dest.Free;
end;

procedure TClosedHash.Insert(const Item: integer);
var
  Index, Origin, Cols, I: integer;
begin
  Cols := 0;
  Index := PFun(Item);
  if PTab[Index] = Item then
    raise Exception.Create(ALREADY_INSERTED)
  else if PTab[Index] >= 0 then
  begin
    Origin := Index;
    I := 1;
    repeat
      if PLinear then
        Index := (Origin + I) mod PSize
      else
        Index := (PFun(Origin) + PCQ * I * I + PCL * I) mod PSize;
      Inc(Cols);
      Inc(I);
    until (PTab[Index] = Item) or (PTab[Index] < 0) or (I = PSize);
    if PTab[Index] = Item then
      raise Exception.Create(ALREADY_INSERTED);
    if I = PSize then
      raise Exception.Create(NO_SPACE_LEFT);
  end;

  PTab[Index] := Item;
  Inc(PCollisions, Cols);
  Inc(PUsed);
  Draw;
end;

procedure TClosedHash.Delete(const Item: integer);
var
  Index, Origin, I: integer;
  Found: boolean = False;
begin
  Index := PFun(Item);
  if PTab[Index] = Item then
    Found := True
  else
  begin
    Origin := Index;
    I := 0;
    repeat
      if PLinear then
        Index := (Origin + I) mod PSize
      else
        Index := (PFun(Origin) + PCQ * I * I + PCL * I) mod PSize;
      Inc(I);
    until (PTab[Index] = EMPTY) or (PTab[Index] = Item) or (I = PSize);
    if PTab[Index] = Item then
      Found := True;
  end;

  if Found then
  begin
    PTab[Index] := Item * -1;
    Dec(PUsed);
    Draw;
  end
  else
    raise Exception.Create(NO_SUCH_NUMBER);
end;

function TClosedHash.Member(const Item: integer): boolean;
var
  Index, Origin, I: integer;
begin
  Result := False;
  Index := PFun(Item);
  if PTab[Index] = Item then
    Result := True
  else
  begin
    Origin := Index;
    I := 0;
    repeat
      if PLinear then
        Index := (Origin + I) mod PSize
      else
        Index := (PFun(Origin) + PCQ * I * I + PCL * I) mod PSize;
      Inc(I);
    until (PTab[Index] = Item) or (I = PSize);
    if PTab[Index] = Item then
      Result := True;
  end;
end;

function TClosedHash.InsertCheck(const Amount: integer): integer;
begin
  if PSize - PUsed >= Amount then
    Result := Amount
  else
    Result := PSize - PUsed;
end;

end.
